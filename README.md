Repository with Jailhouse build configuration for Jetson TX2

Building
========


    wget https://debian.pengutronix.de/debian/pool/main/o/oselas.toolchain/oselas.toolchain-2014.12.2-aarch64-v8a-linux-gnu-gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized_2014.12.2_amd64.deb
    sudo dpkg -i oselas.toolchain-2014.12.2-aarch64-v8a-linux-gnu-gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized_2014.12.2_amd64.deb
    git submodule update --init
    cd build
    make

The above cross-compiles the following: Linux kernel, Jailhouse and
simple root filesystem.

Building with redo
------------------

The above `make` command works well for the first-time compilation,
but fails to track all file dependencies properly. This, if you change
some source code, `make` may not notice it and does not recompile what
is needed.

To overcome this limitation, this project can also be built with
[Apenwarr's redo](https://redo.rtfd.io) build system. Instead of
`make`, in the commands above, run:

    redo

If you want parallel (faster) build, run:

    redo -j$(nproc)

Redo should always recompile what's needed.

Building on NixOS
-----------------

On NixOS, so called FHS environment has to be used. To enter the FHS
environment (and install all dependencies), run `nix-shell` in the
`build` directory. Note that things will not work properly, if `/opt`
directory exists on your root filesystem!

Building on TX2 (only Jailhouse and Linux)
------------------------------------------

    make jailhouse CROSS_COMPILE=

Booting
=======

Network boot
------------

We use novaboot tool (https://github.com/wentasah/novaboot) to boot
the resulting images over network. Use the `build/boot` script to boot
Jailhouse or `build/boot-linux` to boot plain Linux without starting
Jailhouse.

Flash/SD card boot
------------------

To boot Linux and Jailhouse without novaboot, create a Debian package
by running:

	cd build
    make deb

Copy the resulting .deb package to the board and install it with

    dpkg -i jailhouse_*.deb

After rebooting, choose "prem kernel and jailhouse" entry from the
extlinux boot menu. Once the system boots, use `systemctl` command to
start/stop jailhouse:

	systemctl {start|stop|status} jailhouse
