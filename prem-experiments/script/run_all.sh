#!/bin/sh

#scenarios="42mul+1fft+5rand 42mul+1fft+3rand+2rand 42mul+1fft+1fft+3rand 7mul+1fft+5rand"
scenarios="42mul+1fft+5rand 42mul+1fft+1fft+3rand 7mul+1fft+5rand"

#types="ilp legacy mutex"
types="ilp legacy"
iterations=100000

echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "51000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
echo "51000" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
echo "51000" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
echo "51000" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed

sleep 1

echo "1555500" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
echo "1555500" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
echo "1555500" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
echo "1555500" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed

sleep 1

echo "1632000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
echo "1632000" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
echo "1632000" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
echo "1632000" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed

sleep 1

echo "1734000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
echo "1734000" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
echo "1734000" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
echo "1734000" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed

sleep 1

for scenario in $scenarios
do
    for type in $types
    do
        exe="${scenario}_${type}"
        log_name="log_${scenario}_${type}.log"
        echo "=====" > "${log_name}"
        n=0
        while [[ $n -lt $iterations ]]
        do
            nice -n 10 ./"${exe}" >> "${log_name}"
            #echo "=====" >> "${log_name}"
            n=$((n+1))
        done
    done
done
