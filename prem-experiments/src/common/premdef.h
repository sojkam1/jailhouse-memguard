#ifndef PREMDEF_H
#define PREMDEF_H

#include <pthread.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <linux/perf_event.h>
#include "armpmulib.h"

#define SYS_prem_cache_flush 789
#define SYS_prem_irq_enable 790
#define SYS_prem_irq_disable 791
#define SYS_memguard 793

#define PREM_PMC_SIZE 100

#define PREM_JOB_TYPE_COMPATIBLE 0
#define PREM_JOB_TYPE_PREDICTABLE 1

#define CHECK(sts,msg)  \
  if (sts == -1) {      \
    perror(msg);        \
    exit(-1);           \
  }

extern volatile uint32_t ilp_counter;
extern volatile uint32_t memory_lock;

typedef struct prem_function{
    void (*func)();
    uint32_t parallel_id;
    uint32_t kernel_id;
    uint32_t type;
    char name[16];
    int successors[32];
    int successor_quantity;
    volatile int predecesor_done;
    int predecesor_quantity;
    volatile uint32_t tsc[6];
    volatile uint64_t time[6];
    volatile uint32_t miss[6];
    volatile uint32_t core_id;
    volatile uint32_t order_prefetch;
    volatile uint32_t order_writeback;
    volatile uint32_t budget_time_pref;
    volatile uint32_t budget_time_comp;
    volatile uint32_t budget_time_wrba;
    volatile uint32_t budget_memory_pref;
    volatile uint32_t budget_memory_comp;
    volatile uint32_t budget_memory_wrba;
} prem_function;

typedef struct prem_resource{
    pthread_t thread;
    int core;
    uint32_t worker_done;
} prem_resource;

/* Memguard flags */
#define MGF_PERIODIC                                                           \
	(1 << 0) /* Chooses between periodic or one-shot budget replenishment */
#define MGF_MASK_INT                                                           \
	(1                                                                     \
	 << 1) /* Mask (disable) low priority interrupts until next memguard call */

static __attribute__((always_inline)) inline long memguard(unsigned long timeout, unsigned long memory_budget,
	      unsigned long flags)
{
	return syscall(SYS_memguard, timeout, memory_budget, flags);
}

static __attribute__((always_inline)) inline long perf_event_open(struct perf_event_attr *hw_event, pid_t pid, int cpu, int group_fd, unsigned long flags){
    int ret = syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
    return ret;
}

static __attribute__((always_inline)) inline int perf_event_configure(uint32_t type, uint64_t config, int group_fd) {
    struct perf_event_attr pe;
    int fd;

    memset(&pe,0,sizeof(struct perf_event_attr));
    pe.type = type;
    pe.size = sizeof(struct perf_event_attr);
    pe.config = config;
    if (group_fd == -1)
        pe.disabled = 1;
    pe.exclude_kernel = 1;
    pe.exclude_hv = 1;

    fd = perf_event_open(&pe, 0, -1, group_fd, 0);

    return fd;
}

static __attribute__((always_inline)) inline void perf_event_start(int group_fd){
    ioctl(group_fd, PERF_EVENT_IOC_RESET, 0);
    ioctl(group_fd, PERF_EVENT_IOC_ENABLE, 0);
}

static __attribute__((always_inline)) inline void perf_event_reset(int group_fd){
    ioctl(group_fd, PERF_EVENT_IOC_RESET, 0);
}

static __attribute__((always_inline)) inline void perf_event_stop(int group_fd){
    ioctl(group_fd, PERF_EVENT_IOC_DISABLE, 0);
}

static __attribute__((always_inline)) inline uint64_t perf_event_read(int fd){
    int ret;
    uint64_t value;
    ret = read(fd, &value, sizeof(uint64_t));
    
    if(ret != 8){
        return -1;
    }else{
        return value;
    }
}

static __attribute__((always_inline)) inline void killworkers(volatile uint32_t *lock){
    while(!__sync_bool_compare_and_swap(lock, 0, 1)){
        
    }
    __asm volatile("dmb sy":::"memory");
}

static __attribute__((always_inline)) inline void spinlock(volatile uint32_t *lock){
    while(!__sync_bool_compare_and_swap(lock, 0, 1)){
        
    }
    __asm volatile("dmb sy":::"memory");
}

static __attribute__((always_inline)) inline void spinunlock(volatile uint32_t *lock){
    *lock = 0;
    __asm volatile("dmb sy":::"memory");
}
/*
static inline void semwait(volatile uint32_t *lock){
    while(!__sync_bool_compare_and_swap(lock, 1, 0)){

    }
}

static inline void sempost(volatile uint32_t *lock){
    *lock = 1;
}
*/

static __attribute__((always_inline)) inline void quepush(prem_function ** queue, volatile int *back, prem_function * job){
    queue[*back]=job;
    *back += 1;
    __asm volatile("dmb sy":::"memory");
    /*int old_value;
    do{
        old_value = *back;
    }while (!__sync_bool_compare_and_swap(back, old_value, old_value + 1));
    */
}

static __attribute__((always_inline)) inline int quetrywait(volatile int *front, volatile int *back){
    int old_value = *front;
    while(old_value < *back){
        if(__sync_bool_compare_and_swap(front, old_value, old_value + 1)){
            __asm volatile("dmb sy":::"memory");
            return old_value;
        }
        old_value = *front;
    }
    return -1;
}

static __attribute__((always_inline)) inline void semwait(volatile uint32_t *count) {
    uint32_t old_value;
    while(1){
        old_value = *count;
        if(old_value == 0){
            continue;
        }
        if(__sync_bool_compare_and_swap(count, old_value, old_value - 1)){
            break;
        }
    }
    __asm volatile("dmb sy":::"memory");
}

static __attribute__((always_inline)) inline int semtrywait(volatile uint32_t *count) {
    uint32_t old_value;
    do {
        old_value = *count;
        if (old_value == 0){
            return -1;
        }
    } while (!__sync_bool_compare_and_swap(count, old_value, old_value - 1));
    return 0;
}

static __attribute__((always_inline)) inline void sempost(volatile uint32_t *count) {
    uint32_t old_value;
    //printf("%%% sempost executed: %%%\n");
    do{
        //printf("%%% do %%%\n");
        old_value = *count;
    }while (!__sync_bool_compare_and_swap(count, old_value, old_value + 1));
    //printf("%%% sempost finished %%%\n");
    __asm volatile("dmb sy":::"memory");
}

/* Not sure if absolutely safe!! */
static __attribute__((always_inline)) inline void sync_threads_master(volatile uint32_t *worker_ready, volatile uint32_t *worker_done, uint32_t thread_quantity){
    sempost(worker_ready);
//    puts("master");
    while(*worker_ready != thread_quantity);
//    puts("done");
    sempost(worker_done);
    while(*worker_done != thread_quantity);
    *worker_ready = 0;
    *worker_done = 0;
//    puts("zero");
}

static __attribute__((always_inline)) inline void sync_threads_slave(volatile uint32_t *worker_ready, volatile uint32_t *worker_done, uint32_t thread_quantity){
    sempost(worker_ready);
//    puts("we are waiting for affinity");
    while(*worker_ready != thread_quantity);
//    puts("done");
    sempost(worker_done);
}

static inline __attribute__((always_inline)) void flush_address(const void * address){
    /* Or civac? ARM Cortex-A series programmers guide for ARMv8 - 11.5 cache maintenance */
    __asm__ __volatile__("dc civac, %0\n" : : "r" (address) : "memory");
}

static inline __attribute__((always_inline)) void prefetch_address_read(const void * address){
    __asm__ __volatile__("prfm pldl2keep, %a0\n" : : "p" (address));
}

static inline __attribute__((always_inline)) void prefetch_address_write(const void * address){
    __asm__ __volatile__("prfm pstl2keep, %a0\n" : : "p" (address));
}

static inline __attribute__((always_inline)) void print_job(prem_function * job){
    printf("Job: ");
    for(int i=0; i<4; i++){
        printf("%d ",job->tsc[i]);
    }
    for(int i=0; i<4; i++){
        printf("%d ",job->miss[i]);
    }
    puts("");
}

static inline __attribute__((always_inline)) void prem_pred_prefetch(prem_function * job){
    while(ilp_counter < job->order_prefetch);
    //spinlock(&memory_lock);
#if defined(PREM_USE_MEMGUARD)
    memguard(job->budget_time_pref, job->budget_memory_pref, MGF_MASK_INT);
#endif // defined(PREM_USE_MEMGUARD)
    sempost(&ilp_counter);
    job->miss[0] = read_pmu();
    job->tsc[0] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("pref: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_pred_compute(prem_function * job){
    //spinunlock(&memory_lock);
#if defined(PREM_USE_MEMGUARD)
    memguard(job->budget_time_comp, job->budget_memory_comp, MGF_MASK_INT);
#endif // defined(PREM_USE_MEMGUARD)
    job->miss[1] = read_pmu();
    job->tsc[1] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("com: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_pred_writeback(prem_function * job){
    job->miss[2] = read_pmu();
    job->tsc[2] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("com_end: %s %u\n", job->name, ilp_counter);
#endif
#if defined(PREM_USE_MEMGUARD)
    memguard(0, 0, MGF_MASK_INT);
#endif // defined(PREM_USE_MEMGUARD)
    while(ilp_counter < job->order_writeback);
    //spinlock(&memory_lock);
#if defined(PREM_USE_MEMGUARD)
    memguard(job->budget_time_wrba, job->budget_memory_wrba, MGF_MASK_INT);
#endif // defined(PREM_USE_MEMGUARD)
    sempost(&ilp_counter);
    job->miss[3] = read_pmu();
    job->tsc[3] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("wb: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_pred_end(prem_function * job){
    //spinunlock(&memory_lock);
#if defined(PREM_USE_MEMGUARD)
    memguard(0, 0, MGF_MASK_INT);
#endif // defined(PREM_USE_MEMGUARD)
    job->miss[4] = read_pmu();
    job->tsc[4] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("end: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_comp_start(prem_function * job){
    while(ilp_counter < job->order_prefetch);
    //spinlock(&memory_lock);
    sempost(&ilp_counter);
    job->miss[0] = read_pmu();
    job->tsc[0] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("cmp: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_comp_end(prem_function * job){
    //spinunlock(&memory_lock);
    sempost(&ilp_counter);
    job->miss[1] = read_pmu();
    job->tsc[1] = rdtsc32();
#ifdef PRINT_MESSAGES
    printf("cmp_end: %s %u\n", job->name, ilp_counter);
#endif
}

static inline __attribute__((always_inline)) void prem_meas_start(prem_function * job){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    //job->miss[0] = read_pmu();
    job->time[0] = ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
    //job->tsc[0] = rdtsc32();
}

static inline __attribute__((always_inline)) void prem_meas_end(prem_function * job){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    //job->miss[1] = read_pmu();
    job->time[1] = ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
    //job->tsc[1] = rdtsc32();
}

#endif /* PREMDEF_H */
