#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "kernels.h"
#include "sizedef.h"
#include "armpmulib.h"
#include "premdef.h"


/* Global variables */

/* gemm 7 parallel chunks */
#define SIZE_G7_R 512
#define SIZE_G7_C 96
#define SIZE_G7_D 144

double gemm7A[TOTAL_G7][SIZE_G7_R * SIZE_G7_D];
double gemm7B[TOTAL_G7][SIZE_G7_D * SIZE_G7_C];
double gemm7C[TOTAL_G7][SIZE_G7_R * SIZE_G7_C];
double gemm7BB[TOTAL_G7][7][SIZE_G7_D * SIZE_G7_C];

/* gemm 4 parallel chunks */
#define SIZE_G4_R 256
#define SIZE_G4_C 96
#define SIZE_G4_D 144

double gemm4A[TOTAL_G4][SIZE_G4_R * SIZE_G4_D];
double gemm4B[TOTAL_G4][SIZE_G4_D * SIZE_G4_C];
double gemm4C[TOTAL_G4][SIZE_G4_R * SIZE_G4_C];
double gemm4BB[TOTAL_G4][4][SIZE_G4_D * SIZE_G4_C];

/* gemm 2 parallel chunks */
#define SIZE_G2_R 256
#define SIZE_G2_C 80
#define SIZE_G2_D 96

double gemm2A[TOTAL_G2][SIZE_G2_R * SIZE_G2_D];
double gemm2B[TOTAL_G2][SIZE_G2_D * SIZE_G2_C];
double gemm2C[TOTAL_G2][SIZE_G2_R * SIZE_G2_C];
double gemm2BB[TOTAL_G2][2][SIZE_G2_D * SIZE_G2_C];

/* fft */
float fft_array[TOTAL_FFT][ARRAY_SIZE];

/* Jacobian 2D */
#define SIZE_JACOBI 1024
float jacobiA[TOTAL_JACOBI][SIZE_JACOBI][SIZE_JACOBI];
float jacobiB[TOTAL_JACOBI][SIZE_JACOBI][SIZE_JACOBI];

/* Convolution 2D */
#define SIZE_CONV 2048
float convA[TOTAL_CONV][SIZE_CONV][SIZE_CONV];
float convB[TOTAL_CONV][SIZE_CONV][SIZE_CONV];

volatile uint64_t dummy_value;

uint64_t random_values[RANDOM_ARRAY_SIZE];

/* EXTERNAL KERNELS */

/* Jacobi */
void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionLoad(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);
void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionExecute(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);
void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionStore(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);

void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionLoad(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);
void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionExecute(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);
void jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionStore(int32_t phase_start_idx, int64_t phase_max_idx, float A[1024][1024], float B[1024][1024], int32_t * reached_end_idx);

/* Convolution 2D */
void main_dummy___chunk____kernel__for_body13_viCond_WSFunctionLoad(int32_t phase_start_idx, int64_t phase_max_idx, float A[2048][2048], float B[2048][2048], int32_t * reached_end_idx);
void main_dummy___chunk____kernel__for_body13_viCond_WSFunctionExecute(int32_t phase_start_idx, int64_t phase_max_idx, float A[2048][2048], float B[2048][2048], int32_t * reached_end_idx);
void main_dummy___chunk____kernel__for_body13_viCond_WSFunctionStore(int32_t phase_start_idx, int64_t phase_max_idx, float A[2048][2048], float B[2048][2048], int32_t * reached_end_idx);

void init_data(void){
    for(int i = 0; i < TOTAL_G7; i++){
	// gA
	for(int j = 0; j < SIZE_G7_R * SIZE_G7_D; j++){
	   gemm7A[i][j]=j;
	}
	// gB
	for(int j = 0; j < SIZE_G7_D * SIZE_G7_C; j++){
	   gemm7B[i][j]=j;
	}
	// gC
	for(int j = 0; j < SIZE_G7_R * SIZE_G7_C; j++){
	   gemm7C[i][j]=j;
	}
	// gBB
	for(int j = 0; j < 7; j++){
	   for(int k = 0; k < SIZE_G7_D * SIZE_G7_C; k++){
	       gemm7BB[i][j][k]=j-k;
	   }
	}
    }
    for(int i = 0; i < TOTAL_FFT; i++){
	// fft_array
	for(int j = 0; j < ARRAY_SIZE; j++){
	   fft_array[i][j]=j;
	}
    }
    // random_values
    for(int j = 0; j < RANDOM_ARRAY_SIZE; j++){
       random_values[j]=j;
    }
}

void __prem_prefetch(void * ptr, int type){
    static uint64_t last_address = 0;
    uint64_t current_address = (uint64_t)ptr;
    if((last_address >> 6) != (current_address >> 6)){
        if(type)
            __asm__ __volatile__("prfm pldl2keep, %a0\n" : : "p" (ptr));
        else
            __asm__ __volatile__("prfm pstl2keep, %a0\n" : : "p" (ptr));
    }
    last_address = current_address;
    //printf("prefetch: 0x%8X type: %s\n", ptr, type ? "r": "w");
}

void __prem_evict(void * ptr){
    static uint64_t last_address = 0;
    uint64_t current_address = (uint64_t)ptr;
    if((last_address >> 6) != (current_address >> 6)){
        __asm__ __volatile__("dc civac, %0\n" : : "r" (ptr) : "memory");
    }
    last_address = current_address;
    //printf("evict: 0x%8X\n", ptr);
}

void __prem_init(){
    
}

void __prem_notify(){
    
}

void __prem_fini(){
    
}

void __premized__convolution__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 15 * job->parallel_id;
    int32_t to = from + 15;
    
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    main_dummy___chunk____kernel__for_body13_viCond_WSFunctionLoad(from, to, convA[job->kernel_id], convB[job->kernel_id], &end);
    
    /* COMPUTE */
    prem_pred_compute(job);
    
    main_dummy___chunk____kernel__for_body13_viCond_WSFunctionExecute(from, to, convA[job->kernel_id], convB[job->kernel_id], &end);
    
    /* WB */
    prem_pred_writeback(job);
    
    main_dummy___chunk____kernel__for_body13_viCond_WSFunctionStore(from, to, convA[job->kernel_id], convB[job->kernel_id], &end);
    
    prem_pred_end(job);
}

void __premized__jacobi_first__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 31 * job->parallel_id;
    int32_t to = from + 31;
    
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionLoad(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    /* COMPUTE */
    prem_pred_compute(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionExecute(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    /* WB */
    prem_pred_writeback(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionStore(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    prem_pred_end(job);
}

void __premized__jacobi_second__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 32 * job->parallel_id;
    int32_t to = from + 32;
    
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionLoad(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    /* COMPUTE */
    prem_pred_compute(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionExecute(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    /* WB */
    prem_pred_writeback(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionStore(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    prem_pred_end(job);
}

static __attribute__((always_inline)) inline void __premized__random_access(int max_count, prem_function * job) {
    int i;
    uint32_t index;
    uint64_t sum = 0;
    
    prem_comp_start(job);
    
    for(i=0; i<max_count; i++) {
        index = (uint32_t)rand();
        index = index % RANDOM_ARRAY_SIZE;
        sum += random_values[index];
        flush_address(&random_values[index]);
    }
    
    dummy_value = sum;
    
    prem_comp_end(job);
}

void __premized__random__wrapper(prem_function * job){
    if(job->parallel_id == 0){
        __premized__random_access(3800, job);
    }else if(job->parallel_id == 1){
	__premized__random_access(255, job);
    }
}

static __attribute__((always_inline)) inline void __premized__transpose(double * B, double * BB, int size_d, int size_c, prem_function * job) {
    int i,j;
    
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    for(i=0; i<size_d * size_c; i+=4){
        prefetch_address_read(&B[i]);
    }
    for(i=0; i<size_d * size_c; i+=4){
        prefetch_address_write(&BB[i]);
    }
    
    /* COMPUTE */
    prem_pred_compute(job);
    
    for(i=0; i<size_d; i++) {
        for(j=0; j<size_c; j++) {
            BB[j*size_d+i] = B[i*size_c+j];
        }
    }
    
    /* WB */
    prem_pred_writeback(job);
    
    for(i=0; i<size_d * size_c; i+=4){
        flush_address(&B[i]);
    }
    for(i=0; i<size_d * size_c; i+=4){
        flush_address(&BB[i]);
    }
    
    prem_pred_end(job);
    
    /* Duplicate data */
    /*
    for(int i=0; i<4; i++){
        for(int j=0; j<SIZE_D1 * SIZE_C1; j++){
            gBBX[i][j]=gBB[j];
        }
    }
    
    for(int i=0; i<4; i++){
        for(int j=0; j<SIZE_D1 * SIZE_C1; j++){
            flush_address(&gBBX[i][j]);
        }
    }
    */
}

void __premized__gemm_transpose7__wrapper(prem_function * job){
    __premized__transpose(gemm7B[job->kernel_id], gemm7BB[job->kernel_id][0], SIZE_G7_D, SIZE_G7_C, job);
}

void __premized__gemm_transpose4__wrapper(prem_function * job){
    __premized__transpose(gemm4B[job->kernel_id], gemm4BB[job->kernel_id][0], SIZE_G4_D, SIZE_G4_C, job);
}

void __premized__gemm_transpose2__wrapper(prem_function * job){
    __premized__transpose(gemm2B[job->kernel_id], gemm2BB[job->kernel_id][0], SIZE_G2_D, SIZE_G2_C, job);
}

static __attribute__((always_inline)) inline void __premized__gemm(double * A, double * BB, double * C, int size_d, int size_c, int __prem__from, int __prem__to, prem_function * job){
    int i, j, k;
    double sum  = 0;
    //double * lBB = gBBX[job->parallel_id];
#ifdef PRINT_MESSAGES
    printf("Kernel from: %d to: %d \n",__prem__from, __prem__to);
#endif
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    prefetch_address_write(&sum);
    
    for (i = __prem__from * size_c; i < (__prem__to+1) * size_c; i+=4) {
        prefetch_address_write(&C[i]);
    }

    for (i = __prem__from * size_d; i < (__prem__to+1) * size_d; i+=4) {
        prefetch_address_read(&A[i]);
    }

    for (j = 0; j < size_c * size_d; j+=4) {
        prefetch_address_read(&BB[j]);
    }

    /* COMPUTE */
    prem_pred_compute(job);

    for (i = __prem__from; i < __prem__to; i++) {
        for (j = 0; j < size_c; j++) {
            for (k = 0; k < size_d; k++) {
                sum += A[i*size_d+k]*BB[j*size_d+k];
            }
            C[i*size_c+j] = sum;
        }
    }

    /* WB */
    prem_pred_writeback(job);

    flush_address(&sum);
    
    for (i = __prem__from * size_c; i < (__prem__to+1) * size_c; i+=4) {
        flush_address(&C[i]);
    }

    for (i = __prem__from * size_d; i < (__prem__to+1) * size_d; i+=4) {
        flush_address(&A[i]);
    }

    for (j = 0; j < size_c * size_d; j+=4) {
        flush_address(&BB[j]);
    }
    
    prem_pred_end(job);
#ifdef PRINT_MESSAGES
    puts("Lock returned");
#endif
}

void __premized__gemm_kernel7__wrapper(prem_function * job){
    int from = 78*job->parallel_id;
    int to = from + 77;
    if(to>SIZE_G7_R){
        to = SIZE_G7_R - 1;
    }
    __premized__gemm(gemm7A[job->kernel_id], gemm7BB[job->kernel_id][job->parallel_id], gemm7C[job->kernel_id],
        SIZE_G7_D, SIZE_G7_C, from, to, job);
}

void __premized__gemm_kernel4__wrapper(prem_function * job){
    int from = 78*job->parallel_id;
    int to = from + 77;
    if(to>SIZE_G4_R){
        to = SIZE_G4_R - 1;
    }
    __premized__gemm(gemm4A[job->kernel_id], gemm4BB[job->kernel_id][job->parallel_id], gemm4C[job->kernel_id],
        SIZE_G4_D, SIZE_G4_C, from, to, job);
}

void __premized__gemm_kernel2__wrapper(prem_function * job){
    int from = 142*job->parallel_id;
    int to = from + 141;
    if(to>SIZE_G2_R){
        to = SIZE_G2_R - 1;
    }
    __premized__gemm(gemm2A[job->kernel_id], gemm2BB[job->kernel_id][job->parallel_id], gemm2C[job->kernel_id],
        SIZE_G2_D, SIZE_G2_C, from, to, job);
}

#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
static __attribute__((always_inline)) inline double sina(double angle){
    double sin;
    //always wrap input angle to -PI..PI
    if (angle < -PI)
        angle += 2*PI;
    else
        if (angle >  PI)
            angle -= 2*PI;

    //compute sine
    if (angle < 0)
    {
        sin = 1.27323954 * angle + 0.405284735 * angle * angle;

        if (sin < 0)
            sin = 0.225 * (sin *-sin - sin) + sin;
        else
            sin = 0.225 * (sin * sin - sin) + sin;
    }
    else
    {
        sin = 1.27323954 * angle - 0.405284735 * angle * angle;

        if (sin < 0)
            sin = 0.225 * (sin *-sin - sin) + sin;
        else
            sin = 0.225 * (sin * sin - sin) + sin;
    }
    return sin;
}

static __attribute__((always_inline)) inline void __premized__fft(float * data, prem_function * job)
{
    unsigned long mmax,m,j,istep,i;
    double wtemp,wr,wpr,wpi,wi,theta;
    float tempr,tempi;
    //float * data = fft_array-1;
    
    /* PREFETCH */
    prem_pred_prefetch(job);
    
    for(int i=1; i<ARRAY_SIZE+1; i+=8){
        //istep += data[i];
        prefetch_address_write(&data[i]);
    }
    
    /* COMPUTE */
    prem_pred_compute(job);
    
    /* Twiddle bits - ineffective implementation, but bounded */
    for (i=0;i<ARRAY_SIZE_ELEMENTS;i++) {
        m = 0;
        for (int j = 0; j < ARRAY_SIZE_POWER_TWO; j++)
        {
             m |= ((i >> j) & 0x1) << (ARRAY_SIZE_POWER_TWO-j-1);
        }
        if (m > i) {
            SWAP(data[m+m+1],data[i+i+1]);
            SWAP(data[m+m+2],data[i+i+2]);
        }
    }
    mmax=1;
    for(i=0; i < ARRAY_SIZE_POWER_TWO; i++){
        //mmax = powers[i];
        mmax = mmax << 1;
        istep=mmax << 1;
        theta=(6.28318530717959/mmax);
        wtemp=sina(0.5*theta);
        wpr = -2.0*wtemp*wtemp;
        wpi=sina(theta);
        wr=1.0;
        wi=0.0;
        for (m=1;m<mmax;m+=2) {
            for (i=m;i<=ARRAY_SIZE;i+=istep) {
                j=i+mmax;
                tempr=wr*data[j]-wi*data[j+1];
                tempi=wr*data[j+1]+wi*data[j];
                data[j]=data[i]-tempr;
                data[j+1]=data[i+1]-tempi;
                data[i] += tempr;
                data[i+1] += tempi;
            }
            wr=(wtemp=wr)*wpr-wi*wpi+wr;
            wi=wi*wpr+wtemp*wpi+wi;
        }
    }
    
    /* WB */
    prem_pred_writeback(job);
    
    for(int i=-1; i<ARRAY_SIZE-1; i+=8){
        flush_address(&data[i]);
    }
    
    prem_pred_end(job);
    
}
#undef SWAP

void __premized__fft__wrapper(prem_function * job){
    __premized__fft(fft_array[job->kernel_id]-1, job);
}

////////////////////////////////////////////////////////////////////////
//                  LEGACY KERNELS                                    //
////////////////////////////////////////////////////////////////////////

void __legacy__convolution__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 15 * job->parallel_id;
    int32_t to = from + 15;
    
    prem_meas_start(job);
    
    main_dummy___chunk____kernel__for_body13_viCond_WSFunctionExecute(from, to, convA[job->kernel_id], convB[job->kernel_id], &end);
    
    prem_meas_end(job);
}

void __legacy__jacobi_first__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 31 * job->parallel_id;
    int32_t to = from + 31;

    prem_meas_start(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body_viCond_WSFunctionExecute(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);
    
    prem_meas_end(job);
}
#undef SWAP

void __legacy__jacobi_second__wrapper(prem_function * job) {
    int32_t end = -1;
    int32_t from = 32 * job->parallel_id;
    int32_t to = from + 32;
    
    prem_meas_start(job);
    
    jacobi_2d_imper_c_llmain_dummy___chunk____kernel__for_body40_viCond_WSFunctionExecute(from, to, jacobiA[job->kernel_id], jacobiB[job->kernel_id], &end);

    prem_meas_end(job);
}

static __attribute__((always_inline)) inline void __legacy__random_access(int max_count, prem_function * job) {
    int i;
    uint32_t index;
    uint64_t sum = 0;
    
    prem_meas_start(job);
    
    for(i=0; i<max_count; i++) {
        index = (uint32_t)rand();
        index = index % RANDOM_ARRAY_SIZE;
        sum += random_values[index];
    }
    
    dummy_value = sum;
    
    prem_meas_end(job);
}

void __legacy__random__wrapper(prem_function * job){
    if(job->parallel_id == 0){
        __legacy__random_access(3800, job);
    }else if(job->parallel_id == 1){
	__legacy__random_access(255, job);
    }
}

static __attribute__((always_inline)) inline void __legacy__transpose(double * B, double * BB, int size_d, int size_c, prem_function * job) {
    int i,j;
    
    prem_meas_start(job);
    
    for(i=0; i<size_d; i++) {
        for(j=0; j<size_c; j++) {
            BB[j*size_d+i] = B[i*size_c+j];
        }
    }

    prem_meas_end(job);
}

void __legacy__gemm_transpose7__wrapper(prem_function * job){
    __legacy__transpose(gemm7B[job->kernel_id], gemm7BB[job->kernel_id][0], SIZE_G7_D, SIZE_G7_C, job);
}

void __legacy__gemm_transpose4__wrapper(prem_function * job){
    __legacy__transpose(gemm4B[job->kernel_id], gemm4BB[job->kernel_id][0], SIZE_G4_D, SIZE_G4_C, job);
}

void __legacy__gemm_transpose2__wrapper(prem_function * job){
    __legacy__transpose(gemm2B[job->kernel_id], gemm2BB[job->kernel_id][0], SIZE_G2_D, SIZE_G2_C, job);
}

static __attribute__((always_inline)) inline void __legacy__gemm(double * A, double * BB, double * C, int size_d, int size_c, int __prem__from, int __prem__to, prem_function * job){
    int i, j, k;
    double sum  = 0;
    //double * lBB = gBBX[job->parallel_id];
#ifdef PRINT_MESSAGES
    printf("Kernel from: %d to: %d \n",__prem__from, __prem__to);
#endif

    prem_meas_start(job);

    for (i = __prem__from; i < __prem__to; i++) {
        for (j = 0; j < size_c; j++) {
            for (k = 0; k < size_d; k++) {
                sum += A[i*size_d+k]*BB[j*size_d+k];
            }
            C[i*size_c+j] = sum;
        }
    }

    prem_meas_end(job);
    
#ifdef PRINT_MESSAGES
    puts("Lock returned");
#endif
}

void __legacy__gemm_kernel7__wrapper(prem_function * job){
    int from = 78*job->parallel_id;
    int to = from + 77;
    if(to>SIZE_G7_R){
        to = SIZE_G7_R - 1;
    }
    __legacy__gemm(gemm7A[job->kernel_id], gemm7BB[job->kernel_id][job->parallel_id], gemm7C[job->kernel_id],
        SIZE_G7_D, SIZE_G7_C, from, to, job);
}

void __legacy__gemm_kernel4__wrapper(prem_function * job){
    int from = 78*job->parallel_id;
    int to = from + 77;
    if(to>SIZE_G4_R){
        to = SIZE_G4_R - 1;
    }
    __legacy__gemm(gemm4A[job->kernel_id], gemm4BB[job->kernel_id][job->parallel_id], gemm4C[job->kernel_id],
        SIZE_G4_D, SIZE_G4_C, from, to, job);
}

void __legacy__gemm_kernel2__wrapper(prem_function * job){
    int from = 142*job->parallel_id;
    int to = from + 141;
    if(to>SIZE_G2_R){
        to = SIZE_G2_R - 1;
    }
    __legacy__gemm(gemm2A[job->kernel_id], gemm2BB[job->kernel_id][job->parallel_id], gemm2C[job->kernel_id],
        SIZE_G2_D, SIZE_G2_C, from, to, job);
}

#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
static __attribute__((always_inline)) inline void __legacy__fft(float * data, prem_function * job)
{
    unsigned long mmax,m,j,istep,i;
    double wtemp,wr,wpr,wpi,wi,theta;
    float tempr,tempi;
    //float * data = fft_array-1;
    
    prem_meas_start(job);
    
    /* Twiddle bits - ineffective implementation, but bounded */
    for (i=0;i<ARRAY_SIZE_ELEMENTS;i++) {
        m = 0;
        for (int j = 0; j < ARRAY_SIZE_POWER_TWO; j++)
        {
             m |= ((i >> j) & 0x1) << (ARRAY_SIZE_POWER_TWO-j-1);
        }
        if (m > i) {
            SWAP(data[m+m+1],data[i+i+1]);
            SWAP(data[m+m+2],data[i+i+2]);
        }
    }
    mmax=1;
    for(i=0; i < ARRAY_SIZE_POWER_TWO; i++){
        //mmax = powers[i];
        mmax = mmax << 1;
        istep=mmax << 1;
        theta=(6.28318530717959/mmax);
        wtemp=sina(0.5*theta);
        wpr = -2.0*wtemp*wtemp;
        wpi=sina(theta);
        wr=1.0;
        wi=0.0;
        for (m=1;m<mmax;m+=2) {
            for (i=m;i<=ARRAY_SIZE;i+=istep) {
                j=i+mmax;
                tempr=wr*data[j]-wi*data[j+1];
                tempi=wr*data[j+1]+wi*data[j];
                data[j]=data[i]-tempr;
                data[j+1]=data[i+1]-tempi;
                data[i] += tempr;
                data[i+1] += tempi;
            }
            wr=(wtemp=wr)*wpr-wi*wpi+wr;
            wi=wi*wpr+wtemp*wpi+wi;
        }
    }
    
    prem_meas_end(job);
    
}
#undef SWAP

void __legacy__fft__wrapper(prem_function * job){
    __legacy__fft(fft_array[job->kernel_id]-1, job);
}
