#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <string.h>
#include <time.h>
#include "workers.h"
#include "armpmulib.h"
#include "prem-a57-events.h"
#include "kernels.h"
#include "premdef.h"

//#define PRINT_MESSAGES

sem_t work_semaphore;

pthread_mutex_t sched_lock;

const int affinity[4] = {0, 3, 4, 5};

//volatile uint32_t thread_barrier_ready = 0;
//volatile uint32_t thread_barrier_done = 0;
volatile uint32_t affinity_set = 0;

void init_default_prem(void){
    pthread_t thread;
    cpu_set_t cset;
    thread = pthread_self();
    CPU_ZERO(&cset);
    CPU_SET(affinity[0], &cset);                                      /* set nth bit (nth core) */
    pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cset);    /* set affinity of this thread */
    
    init_data();
    
    /* Init memory lock */
    memory_lock = 0;
    /* Init all resources */
    for(int i = 0; i < prem_core_q; i++){
        prem_resources[i].worker_done = 0;
        prem_resources[i].core = affinity[i];
    }
    /* First thread is default thread */
    for(int i = 1; i < prem_core_q; i++){
        pthread_create(&prem_resources[i].thread, NULL, __prem__ilp_worker_thread, (void *)&prem_resources[i]);
    }
}

void init_default_mutex(void){
    pthread_t thread;
    cpu_set_t cset;
    thread = pthread_self();
    CPU_ZERO(&cset);
    CPU_SET(affinity[0], &cset);                                      /* set nth bit (nth core) */
    pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cset);    /* set affinity of this thread */
    
    init_data();

    pthread_mutex_init(&sched_lock, NULL);
    
    /* Init memory lock */
    memory_lock = 0;
    /* Init all resources */
    for(int i = 0; i < prem_core_q; i++){
        prem_resources[i].worker_done = 0;
        prem_resources[i].core = affinity[i];
    }
    /* First thread is default thread */
    for(int i = 1; i < prem_core_q; i++){
        pthread_create(&prem_resources[i].thread, NULL, __prem__mutex_worker_thread, (void *)&prem_resources[i]);
    }
}

void init_default_legacy(){
    
    struct sched_param param;
    int sts;
    int high_priority = sched_get_priority_max(SCHED_FIFO);
    //int high_priority = sched_get_priority_max(SCHED_OTHER);
    CHECK(high_priority,"sched_get_priority_max");

    sts = sched_getparam(0, &param);
    CHECK(sts,"sched_getparam");

    param.sched_priority = high_priority;
    sts = sched_setscheduler(0, SCHED_FIFO, &param);
    //sts = sched_setscheduler(0, SCHED_OTHER, &param);
    CHECK(sts,"sched_setscheduler");
    
    init_data();
    
    /* Init memory lock */
    sem_init(&work_semaphore, 0, 0);
    /* Init all resources */
    for(int i = 0; i < prem_core_q; i++){
        prem_resources[i].worker_done = 0;
        prem_resources[i].core = affinity[i];
    }
    
    pthread_mutex_init(&sched_lock, NULL);
    
    /* First thread is default thread */
    for(int i = 1; i < prem_core_q; i++){
        pthread_create(&prem_resources[i].thread, NULL, __prem__legacy_worker_thread, (void *)&prem_resources[i]);
    }
}

void insert_ilp_tasks(void){
    /* Insert ready tasks into queue */
    for(int j=0; j<prem_job_q*2; j++){
        for(int i=0; i<prem_job_q; i++){
            if(jobs[i].order_prefetch == j){
                quepush(jobs_queue, &jobs_back, &jobs[i]);
#ifdef PRINT_MESSAGES
                printf("Pushing job %d: %s\n", i, jobs[i].name);
#endif
            }
        }
    }
}

void insert_legacy_tasks(void){
    /* Insert ready tasks into queue */
    for(int i=0; i<prem_job_q; i++){
        if(jobs[i].predecesor_quantity == 0){
            quepush(jobs_queue, &jobs_back, &jobs[i]);
            sem_post(&work_semaphore);
#ifdef PRINT_MESSAGES
            printf("Pushing job %d: %s\n", i, jobs[i].name);
#endif
        }
    }
}

void * __prem__ilp_worker_thread(void * arg){
    prem_resource * resource = (prem_resource*) arg;
    pthread_t thread;
    cpu_set_t cset;

    thread = pthread_self();
    CPU_ZERO(&cset);
    CPU_SET(resource->core, &cset);                         /* set nth bit (nth core) */
    pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cset);    /* set affinity of this thread */

    //sync_threads_slave(&thread_barrier_ready, &thread_barrier_done, prem_core_q);

#if defined(PREM_USE_MEMGUARD)
    memguard(0, 0, MGF_MASK_INT);
#else
    syscall(SYS_prem_irq_disable);
#endif // defined(PREM_USE_MEMGUARD)
#ifdef PRINT_MESSAGES
    puts("IRQ disabled");
#endif

    sempost(&affinity_set);

    //sync_threads_slave(&thread_barrier_ready, &thread_barrier_done, prem_core_q);

    /* Synchronize counters with other workers */
    while(!__sync_bool_compare_and_swap(&sync_counters, 1, 1));
    enable_pmu(L2D_CACHE_REFILL);
    
    /* Wait for new job in queue */
    while(1) {
#ifdef PRINT_MESSAGES
        printf("Worker %d is going to wait for jobs\n", resource->core);
#endif
        int current_job_position = quetrywait(&jobs_front, &jobs_back);
        while(current_job_position < 0){
            if(jobs_done == prem_job_q){
#if defined(PREM_USE_MEMGUARD)
                memguard(0, 0, 0);
#else
                syscall(SYS_prem_irq_enable);
#endif // defined(PREM_USE_MEMGUARD)
#ifdef PRINT_MESSAGES
                puts("Killing worker");
#endif
                return NULL;
            }
            current_job_position = quetrywait(&jobs_front, &jobs_back);
        }
        
        prem_function * current_job = jobs_queue[current_job_position];
        
#ifdef PRINT_MESSAGES
        printf("Worker %d is processing job %d %s\n", resource->core, current_job_position, current_job->name);
#endif
        
        /* Call kernel */
        (*current_job->func)(current_job);

#ifdef PRINT_MESSAGES
        printf("Worker %d finished %d %s\n", resource->core, current_job_position, current_job->name);
#endif
        
        /* Increment job counter */
        sempost(&jobs_done);
    }
}

void __prem__ilp_default_thread(prem_resource * resource){
    while(1){
        int current_job_position = quetrywait(&jobs_front, &jobs_back);
        while(current_job_position < 0){
            if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
                puts("Quit default thread");
#endif
                return;
            }
            current_job_position = quetrywait(&jobs_front, &jobs_back);
        }
        
        prem_function * current_job = jobs_queue[current_job_position];
#ifdef PRINT_MESSAGES
        printf("Default worker is processing job %d %s from %d to %d\n", current_job_position, current_job->name);
#endif
        (*current_job->func)(current_job);

#ifdef PRINT_MESSAGES
        printf("Default worker %d finished %d %s from %d to %d\n", resource->core, current_job_position, current_job->name);
#endif        

        /* Increment job counter */
        sempost(&jobs_done);
    }
}

void __prem__legacy_default_thread(prem_resource * resource){
    int pmc_cycles_fd = perf_event_configure(PERF_TYPE_RAW, CPU_CYCLES, -1);
    int pmc_inst_fd = perf_event_configure(PERF_TYPE_RAW, INST_RETIRED, pmc_cycles_fd);
    int pmc_mem_fd = perf_event_configure(PERF_TYPE_RAW, MEM_ACCESS, pmc_cycles_fd);
    int pmc_miss_fd = perf_event_configure(PERF_TYPE_RAW, L2D_CACHE_REFILL, pmc_cycles_fd);
    while(1){
        sem_wait(&work_semaphore);
        if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
            puts("Quit default thread");
#endif
            return;
        }
        
        pthread_mutex_lock(&sched_lock);
        /* Select random task in a queue */
        uint32_t ready_count = jobs_back - jobs_front;
        int selected_job_position = jobs_front + rand() % ready_count;
        
        /* Move to the front position */
        prem_function * tmp_job = jobs_queue[jobs_front];
        jobs_queue[jobs_front] = jobs_queue[selected_job_position];
        jobs_queue[selected_job_position] = tmp_job;
        
        /* Take the job */
        int current_job_position = jobs_front;
        jobs_front++;
        pthread_mutex_unlock(&sched_lock);
        
        //printf("f: %d b: %d c: %d\n", jobs_front, jobs_back, selected_job_position);
        
        prem_function * current_job = jobs_queue[current_job_position];
#ifdef PRINT_MESSAGES
        printf("Default worker is processing job %d %s from %d to %d\n", current_job_position, current_job->name);
#endif
        
        perf_event_reset(pmc_cycles_fd);
        perf_event_reset(pmc_inst_fd);
        perf_event_reset(pmc_mem_fd);
        perf_event_reset(pmc_miss_fd);
        
        perf_event_start(pmc_cycles_fd);
        
        (*current_job->func)(current_job);
        
        perf_event_stop(pmc_cycles_fd);
        
        current_job->time[2] = perf_event_read(pmc_cycles_fd);
        current_job->time[3] = perf_event_read(pmc_inst_fd);
        current_job->time[4] = perf_event_read(pmc_mem_fd);
        current_job->time[5] = perf_event_read(pmc_miss_fd);
        
        /* Schedule other tasks */
        pthread_mutex_lock(&sched_lock);
        for(int i=0; i<current_job->successor_quantity; i++){
            prem_function * successor = &jobs[current_job->successors[i]];
            successor->predecesor_done++;
            if(successor->predecesor_done == successor->predecesor_quantity){
                quepush(jobs_queue, &jobs_back, successor);
                sem_post(&work_semaphore);
#ifdef PRINT_MESSAGES
                printf("Pushing job %d: %s\n", current_job->successors[i], successor->name);
#endif
            }
        }
        pthread_mutex_unlock(&sched_lock);
        
        sempost(&jobs_done);
        
        if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
            puts("All jobs done, inform other threads");
#endif
            for(int i=0; i<prem_core_q; i++){
                sem_post(&work_semaphore);
            }
        }
    }
}

void * __prem__legacy_worker_thread(void * arg){
#ifdef PRINT_MESSAGES
    prem_resource * resource = (prem_resource*) arg;
#endif
    
    int pmc_cycles_fd = perf_event_configure(PERF_TYPE_RAW, CPU_CYCLES, -1);
    int pmc_inst_fd = perf_event_configure(PERF_TYPE_RAW, INST_RETIRED, pmc_cycles_fd);
    int pmc_mem_fd = perf_event_configure(PERF_TYPE_RAW, MEM_ACCESS, pmc_cycles_fd);
    int pmc_miss_fd = perf_event_configure(PERF_TYPE_RAW, L2D_CACHE_REFILL, pmc_cycles_fd);
    
    //enable_pmu(L2D_CACHE_REFILL);
    
    /* Wait for new job in queue */
    while(1) {
#ifdef PRINT_MESSAGES
        printf("Worker %d is going to wait for jobs\n", resource->core);
#endif
        
        sem_wait(&work_semaphore);
        if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
            puts("Kill worker");
#endif
            return NULL;
        }
        
        pthread_mutex_lock(&sched_lock);
        /* Select random task in a queue */
        uint32_t ready_count = jobs_back - jobs_front;
        int selected_job_position = jobs_front + rand() % ready_count;
        
        /* Move to the front position */
        prem_function * tmp_job = jobs_queue[jobs_front];
        jobs_queue[jobs_front] = jobs_queue[selected_job_position];
        jobs_queue[selected_job_position] = tmp_job;
        
        /* Take the job */
        int current_job_position = jobs_front;
        jobs_front++;
        pthread_mutex_unlock(&sched_lock);
        
        prem_function * current_job = jobs_queue[current_job_position];
        
#ifdef PRINT_MESSAGES
        printf("Worker %d is processing job %d %s from %d to %d\n", resource->core, current_job_position, current_job->name);
#endif
        
        perf_event_reset(pmc_cycles_fd);
        perf_event_reset(pmc_inst_fd);
        perf_event_reset(pmc_mem_fd);
        perf_event_reset(pmc_miss_fd);
        
        perf_event_start(pmc_cycles_fd);
        
        /* Call kernel */
        (*current_job->func)(current_job);
        
        perf_event_stop(pmc_cycles_fd);
        
        current_job->time[2] = perf_event_read(pmc_cycles_fd);
        current_job->time[3] = perf_event_read(pmc_inst_fd);
        current_job->time[4] = perf_event_read(pmc_mem_fd);
        current_job->time[5] = perf_event_read(pmc_miss_fd);
        
        /* Schedule subsequent tasks */
        pthread_mutex_lock(&sched_lock);
        for(int i=0; i<current_job->successor_quantity; i++){
            prem_function * successor = &jobs[current_job->successors[i]];
            successor->predecesor_done++;
            if(successor->predecesor_done == successor->predecesor_quantity){
                quepush(jobs_queue, &jobs_back, successor);
                sem_post(&work_semaphore);
#ifdef PRINT_MESSAGES
                printf("Pushing job %d: %s\n", current_job->successors[i], successor->name);
#endif
            }
        }
        pthread_mutex_unlock(&sched_lock);
        sempost(&jobs_done);
        if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
            puts("All jobs done, inform other threads");
#endif
            for(int i=0; i<prem_core_q; i++){
                sem_post(&work_semaphore);
            }
        }
    }
}

void * __prem__mutex_worker_thread(void * arg){
    prem_resource * resource = (prem_resource*) arg;
    pthread_t thread;
    cpu_set_t cset;

    thread = pthread_self();
    CPU_ZERO(&cset);
    CPU_SET(resource->core, &cset);                         /* set nth bit (nth core) */
    pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cset);    /* set affinity of this thread */

#if defined(PREM_USE_MEMGUARD)
    memguard(0, 0, MGF_MASK_INT);
#else
    syscall(SYS_prem_irq_disable);
#endif // defined(PREM_USE_MEMGUARD)
#ifdef PRINT_MESSAGES
    puts("IRQ disabled");
#endif

    /* Synchronize counters with other workers */
    while(!__sync_bool_compare_and_swap(&sync_counters, 1, 1));
    enable_pmu(L2D_CACHE_REFILL);
    
    /* Wait for a job in queue */
    while(1) {
#ifdef PRINT_MESSAGES
        printf("Worker %d is going to wait for jobs\n", resource->core);
#endif
        int current_job_position = quetrywait(&jobs_front, &jobs_back);
        while(current_job_position < 0){
            if(jobs_done == prem_job_q){
#if defined(PREM_USE_MEMGUARD)
                memguard(0, 0, 0);
#else
                syscall(SYS_prem_irq_enable);
#endif // defined(PREM_USE_MEMGUARD)
#ifdef PRINT_MESSAGES
                puts("Killing worker");
#endif
                return NULL;
            }
            current_job_position = quetrywait(&jobs_front, &jobs_back);
        }
        
        prem_function * current_job = jobs_queue[current_job_position];
      
        /* Do the job */
#ifdef PRINT_MESSAGES
        printf("Worker %d is processing job %d %s from %d to %d\n", resource->core, current_job_position, current_job->name);
#endif
        (*current_job->func)(current_job);
        
        /* Add subsequent jobs into queue */
        pthread_mutex_lock(&sched_lock);
        for(int i=0; i<current_job->successor_quantity; i++){
            prem_function * successor = &jobs[current_job->successors[i]];
            successor->predecesor_done++;
            if(successor->predecesor_done == successor->predecesor_quantity){
                quepush(jobs_queue, &jobs_back, successor);
#ifdef PRINT_MESSAGES
                printf("Pushing job %d: %s\n", current_job->successors[i], successor->name);
#endif
            }
        }
        pthread_mutex_unlock(&sched_lock);
        sempost(&jobs_done);
    }
}

void __prem__mutex_default_thread(prem_resource * resource){
    while(1){
        /* Wait for a job in queue */
        int current_job_position = quetrywait(&jobs_front, &jobs_back);
        while(current_job_position < 0){
            if(jobs_done == prem_job_q){
#ifdef PRINT_MESSAGES
                puts("Quit default thread");
#endif
                return;
            }
            current_job_position = quetrywait(&jobs_front, &jobs_back);
        }
        
        prem_function * current_job = jobs_queue[current_job_position];
        
        /* Do the job */
#ifdef PRINT_MESSAGES
        printf("Default worker is processing job %d %s from %d to %d\n", current_job_position, current_job->name);
#endif
        (*current_job->func)(current_job);
        
        /* Add subsequent jobs into queue */
        pthread_mutex_lock(&sched_lock);
        for(int i=0; i<current_job->successor_quantity; i++){
            prem_function * successor = &jobs[current_job->successors[i]];
            successor->predecesor_done++;
            if(successor->predecesor_done == successor->predecesor_quantity){
                quepush(jobs_queue, &jobs_back, successor);
#ifdef PRINT_MESSAGES
                printf("Pushing job %d: %s\n", current_job->successors[i], successor->name);
#endif
            }
        }
        pthread_mutex_unlock(&sched_lock);
        sempost(&jobs_done);
    }
    
}
