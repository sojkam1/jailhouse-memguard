#ifndef KERNELS_H
#define KERNELS_H

#include "premdef.h"

void init_data(void);

void __premized__gemm_transpose7__wrapper(prem_function * job);
void __premized__gemm_kernel7__wrapper(prem_function * job);

void __premized__gemm_transpose4__wrapper(prem_function * job);
void __premized__gemm_kernel4__wrapper(prem_function * job);

void __premized__gemm_transpose2__wrapper(prem_function * job);
void __premized__gemm_kernel2__wrapper(prem_function * job);

void __premized__convolution__wrapper(prem_function * job);

void __premized__jacobi_first__wrapper(prem_function * job);
void __premized__jacobi_second__wrapper(prem_function * job);

void __premized__random__wrapper(prem_function * job);

void __premized__fft__wrapper(prem_function * job);

void __legacy__gemm_transpose7__wrapper(prem_function * job);
void __legacy__gemm_kernel7__wrapper(prem_function * job);

void __legacy__gemm_transpose4__wrapper(prem_function * job);
void __legacy__gemm_kernel4__wrapper(prem_function * job);

void __legacy__gemm_transpose2__wrapper(prem_function * job);
void __legacy__gemm_kernel2__wrapper(prem_function * job);

void __legacy__convolution__wrapper(prem_function * job);

void __legacy__jacobi_first__wrapper(prem_function * job);
void __legacy__jacobi_second__wrapper(prem_function * job);

void __legacy__random__wrapper(prem_function * job);

void __legacy__fft__wrapper(prem_function * job);


#endif /* KERNELS_H */
