#ifndef WORKERS_H
#define WORKERS_H

#include "premdef.h"

extern prem_resource prem_resources[];

extern const int prem_core_q;

extern volatile uint32_t memory_lock;
extern volatile uint32_t kill_workers;
extern volatile uint32_t sync_counters;

extern volatile uint32_t ilp_counter;
extern volatile uint32_t jobs_done;

extern volatile int jobs_front;
extern volatile int jobs_back;

extern prem_function jobs[];

extern prem_function * jobs_queue[];

extern const int prem_job_q;

void init_default_prem(void);
void init_default_legacy(void);
void init_default_mutex(void);

void insert_ilp_tasks(void);
void insert_legacy_tasks(void);

void * __prem__ilp_worker_thread(void * arg);
void __prem__ilp_default_thread(prem_resource * resource);
void * __prem__legacy_worker_thread(void * arg);
void __prem__legacy_default_thread(prem_resource * resource);
void * __prem__mutex_worker_thread(void * arg);
void __prem__mutex_default_thread(prem_resource * resource);

#endif //WORKERS_H
