#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <sys/syscall.h>
#include <string.h>
#include <time.h>
#include "armpmulib.h"
#include "prem-a57-events.h"
#include "kernels.h"
#include "premdef.h"
#include "workers.h"
#include "sizedef.h"

#define FILL_MATRICES

//#define PRINT_ALL
//#define PREM_USE_MEMGUARD

const double time_divider = 2035.200;
//const time_divider = 1000;

/*
extern prem_function trans1;

prem_function trans1 = {
    .name = "trans1",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 6,
    .successors = { &gemm1_1, 2, 3, 4, 5, 6 },
    .from = 0,
    .to = 0,
    .func = __premized__transpose1,
    
};

prem_function gemm1_1 = { ...};
*/

#if defined(PREM_TARGET_MUTEX)
#define PREM_CORES 1
#elif defined(PREM_TARGET_ILP)
#define PREM_CORES 4
#elif defined(PREM_TARGET_LEGACY)
#define PREM_CORES 16
#else
#error Unknown PREM target
#endif

prem_resource prem_resources[PREM_CORES];

const int prem_core_q = PREM_CORES;

volatile uint32_t memory_lock = 0;
volatile uint32_t kill_workers = 0;
volatile uint32_t sync_counters = 0;

volatile uint32_t ilp_counter = 0;
volatile uint32_t jobs_done = 0;

#if defined(PREM_TARGET_ILP)
extern volatile uint32_t affinity_set;
#endif

volatile int jobs_front;
volatile int jobs_back;

extern prem_function jobs[];

extern const int prem_job_q;

#define MGRET_MEM_POS		0
#define MGRET_TIM_POS		32
#define MGRET_OVER_MEM_POS	61
#define MGRET_OVER_TIM_POS	62
#define MGRET_ERROR_POS		63

#define MGRET_TIM_MASK		(0x00FFFFFFul << MGRET_TIM_POS)
#define MGRET_MEM_MASK		(0xFFFFFFFFul << MGRET_MEM_POS)
#define MGRET_OVER_MEM_MASK	(1ul << MGRET_OVER_MEM_POS)
#define MGRET_OVER_TIM_MASK	(1ul << MGRET_OVER_TIM_POS)
#define MGRET_ERROR_MASK	(1ul << MGRET_ERROR_POS)

struct mg_ret {
    uint32_t time;
    uint32_t mem;
    bool time_ovf;
    bool mem_ovf;
};

static struct mg_ret mgret(uint64_t retval)
{
    struct mg_ret mgr = {};
    mgr.time = (retval & MGRET_TIM_MASK) >> MGRET_TIM_POS;
    mgr.mem = (retval & MGRET_MEM_MASK) >> MGRET_MEM_POS;
    mgr.time_ovf = (retval >> MGRET_OVER_TIM_POS) & 1;
    mgr.mem_ovf = (retval >> MGRET_OVER_MEM_POS) & 1;
    return mgr;
}

int main(int argc, char *argv[])
{
    int i;
    //uint32_t tsc_start;
    //uint32_t tsc_stop;
    prem_resource * resource = &prem_resources[0];
    char bla[1];
    struct timespec time_start;
    struct timespec time_stop;
    int pos = 0;
    printf("Now pos is: %d\n", ++pos);

#if defined(PREM_TARGET_MUTEX)
    init_default_mutex();
#elif defined(PREM_TARGET_ILP)
    init_default_prem();
#elif defined(PREM_TARGET_LEGACY)
    init_default_legacy();
#endif
    
    
    (void)rand();

    mlockall(MCL_CURRENT | MCL_FUTURE);
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Waggressive-loop-optimizations"
    #pragma GCC diagnostic ignored "-Wunused-variable"
    for(i=0; i<12; i++){
        volatile char x = bla[0-4096*i];
    }
    printf("Now pos is: %d\n", ++pos);
    #pragma GCC diagnostic pop

#if defined(PREM_TARGET_ILP)
    printf("Set affinity_set == 3\n");
    /* Wait for all threads */
    while(affinity_set != 3);
    printf("Set affinity_set == 3\n");
#endif

    printf("Now pos is: %d\n", ++pos);
    
#if defined(PREM_TARGET_ILP) || defined(PREM_TARGET_MUTEX)
    printf("Clean cache\n");
    /* Disable interrupts and clean cache */
#if defined(PREM_USE_MEMGUARD)
    printf("Set memguard\n");
    long retval = memguard(50, 100, MGF_MASK_INT);
    struct mg_ret r = mgret(retval);
	printf("Testing ⇒ time:%8u%c mem:%8u%c\" in %s:\n",
	       r.time, r.time_ovf ? '!' : ' ',
	       r.mem, r.mem_ovf ? '!' : ' ',
	       __FILE__);
#else
    printf("try SYS_prem_irq_disable\n");
    (void)syscall(SYS_prem_irq_disable);
    printf("tried SYS_prem_irq_disable\n");
#endif // defined(PREM_USE_MEMGUARD)
    printf("try SYS_prem_cache_flush\n");
    (void)syscall(SYS_prem_cache_flush);
    printf("tried SYS_prem_cache_flush\n");
#endif
    printf("Now pos is: %d\n", ++pos);
    
#if defined(PREM_TARGET_LEGACY) || defined(PREM_TARGET_MUTEX)
    /* Insert jobs ready at time 0 */
    insert_legacy_tasks();
    printf("Leagacy task was inserted\n");
#elif defined(PREM_TARGET_ILP)
    /* Order is already predefined, enqueue all tasks */
    insert_ilp_tasks();
#endif
    
#if defined(PREM_TARGET_MUTEX)
    ilp_counter = prem_job_q * 2;
#endif
    printf("Start get time\n");
    clock_gettime(CLOCK_MONOTONIC, &time_start);
    
#if defined(PREM_TARGET_ILP) || defined(PREM_TARGET_MUTEX)
    /* Sync performance counters */
    sync_counters = 1;
    __asm volatile("dmb sy":::"memory");
#endif
    enable_pmu(L2D_CACHE_REFILL);
    
    //tsc_start = rdtsc32();

//    for (int i = 0; i < 10; i++) {
//    printf("memguard: %ld\n", memguard(3, 6, MGF_MASK_INT));
        /* Do part of the work */
#if defined(PREM_TARGET_MUTEX)
        __prem__mutex_default_thread(resource);
#elif defined(PREM_TARGET_ILP)
        __prem__ilp_default_thread(resource);
#elif defined(PREM_TARGET_LEGACY)
        //    for (int i = 0; i < prem_core_q; i++) {
        //        printf("%d %zu\n", prem_resources[i].core, prem_resources[i].worker_done);
        //    }
//            printf("Resources: CPUs %d, worker_done %zu\n", resource->core, resource->worker_done);
            __prem__legacy_default_thread(resource);
//            printf("After ececution resources: CPUs %d, worker_done %zu\n", resource->core, resource->worker_done);
#endif

        printf("Now pos is: %d\n", ++pos);


        /* We do not need to wait for other threads
         * since all tasks already finished
         * (TODO: threads might not be destroyed yet) */

        //tsc_stop = rdtsc32();

        clock_gettime(CLOCK_MONOTONIC, &time_stop);

#if defined(PREM_TARGET_ILP) || defined(PREM_TARGET_MUTEX)
#if defined(PREM_USE_MEMGUARD)
        memguard(0, 0, 0);
#else
        syscall(SYS_prem_irq_enable);
#endif // defined(PREM_USE_MEMGUARD)
#endif

        /* Print results */
//#if defined(PREM_TARGET_ILP) || defined(PREM_TARGET_MUTEX)
        printf("Now pos is: %d\n", ++pos);
        printf("%lf\n", ((time_stop.tv_sec * 1000000 + time_stop.tv_nsec / 1000) -
                         (time_start.tv_sec * 1000000 + time_start.tv_nsec / 1000)) * 0.001);
        printf("Now pos is: %d\n", ++pos);
//        printf("memguard: %ld\n", memguard(0, 0, 0));
//#elif defined(PREM_TARGET_LEGACY)
//    uint64_t min, max;
//    min = UINT64_MAX;
//    max = 0;
//    for(int i=0; i<prem_job_q; i++){
//        prem_function * job = &jobs[i];
//        if(job->time[0] < min){
//            min = job->time[0];
//        }
//        if(job->time[1] > max){
//            max = job->time[1];
//        }
//    }

//    printf("%lf\n", (max-min)*0.001);
//#endif

#if defined(PRINT_ALL)
        printf("num       ");
        for(int i=0; i<prem_job_q; i++){
            if(i<prem_job_q-1){
                printf("%7d, ", i);
            }else{
                printf("%7d\n", i);
            }
        }
        printf("s_pref = [");
        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            if(i<prem_job_q-1){
                printf("%7d, ", (int)((job->tsc[0])/time_divider));
            }else{
                printf("%7d]\n", (int)((job->tsc[0])/time_divider));
            }
        }
        printf("p_pref = [");
        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            if(i<prem_job_q-1){
                printf("%7d, ", (int)((job->tsc[1] - job->tsc[0])/time_divider));
            }else{
                printf("%7d]\n", (int)((job->tsc[1] - job->tsc[0])/time_divider));
            }
        }
        printf("p_comp = [");
        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            if(i<prem_job_q-1){
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d, ", (int)((job->tsc[2] - job->tsc[1])/time_divider));
                }else{
                    printf("%7d, ", 0);
                }
            }else{
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d]\n", (int)((job->tsc[2] - job->tsc[1])/time_divider));
                }else{
                    printf("%7d]\n", 0);
                }
            }
        }
        printf("p_wrba = [");
        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            if(i<prem_job_q-1){
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d, ", (int)((job->tsc[4] - job->tsc[3])/time_divider));
                }else{
                    printf("%7d, ", 1);
                }
            }else{
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d]\n", (int)((job->tsc[4] - job->tsc[3])/time_divider));
                }else{
                    printf("%7d]\n", 1);
                }
            }
        }
        printf("c_miss = [");
        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            if(i<prem_job_q-1){
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d, ", (job->miss[2] - job->miss[1]));
                    //printf("%10d, ", (int64_t)job->miss[2]);
                }else{
                    printf("%7d, ", (job->miss[1] - job->miss[0]));
                    //printf("%10d, ", (int64_t)job->miss[1]);
                }
            }else{
                if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                    printf("%7d]\n", (job->miss[2] - job->miss[1]));
                    //printf("%10d]\n", (int64_t)job->miss[2]);
                }else{
                    printf("%7d]\n", (job->miss[1] - job->miss[0]));
                    //printf("%10d]\n", (int64_t)job->miss[1]);
                }
            }
        }

        /*
        //printf("%d\n", tsc_stop-tsc_start);
        printf("%lf\n", ((time_stop.tv_sec * 1000000 + time_stop.tv_nsec / 1000)-(time_start.tv_sec * 1000000 + time_start.tv_nsec / 1000))*0.001);

        for(int i=0; i<prem_job_q; i++){
            prem_function * job = &jobs[i];
            printf("%d\n", i);
            if(job->type == PREM_JOB_TYPE_PREDICTABLE){
                for(int j=1; j<5; j++){
                    printf("%10u %10u %10u %10u\n", (int)(job->tsc[j-1]/time_divider), (int)(job->tsc[j]/time_divider), (int)((job->tsc[j] - job->tsc[j-1])/time_divider), job->miss[j] - job->miss[j-1]);
                }
            }else{
                printf("%10u %10u %10u %10u\n", (int)(job->tsc[0]/time_divider), (int)(job->tsc[1]/time_divider), (int)((job->tsc[1] - job->tsc[0])/time_divider), job->miss[1] - job->miss[0]);
            }
        }
        */
#endif // defined(PRINT_ALL)
//    }
    return 0;
}
