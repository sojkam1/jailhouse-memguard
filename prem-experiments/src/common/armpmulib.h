#ifndef ARMPMU_LIB_H
#define ARMPMU_LIB_H

static inline uint32_t rdtsc32(void){
#if defined(__GNUC__) && defined __aarch64__
    uint32_t r = 0;   
	__asm__ __volatile__("mrs %0, pmccntr_el0" : "=r" (r)); 
    return r;
#else
#error Unsupported architecture/compiler!
#endif
}

#define ARMV8_PMEVTYPER_P              (1 << 31) /* EL1 modes filtering bit */
#define ARMV8_PMEVTYPER_U              (1 << 30) /* EL0 filtering bit */
#define ARMV8_PMEVTYPER_NSK            (1 << 29) /* Non-secure EL1 (kernel) modes filtering bit */
#define ARMV8_PMEVTYPER_NSU            (1 << 28) /* Non-secure User mode filtering bit */
#define ARMV8_PMEVTYPER_NSH            (1 << 27) /* Non-secure Hyp modes filtering bit */
#define ARMV8_PMEVTYPER_M              (1 << 26) /* Secure EL3 filtering bit */
#define ARMV8_PMEVTYPER_MT             (1 << 25) /* Multithreading */
#define ARMV8_PMEVTYPER_EVTCOUNT_MASK  0x3ff

#define ARMV8_PMCR_E                    (1 << 0) /* Enable */
#define ARMV8_PMCR_P                    (1 << 1) /* Event counter reset */
#define ARMV8_PMCR_C                    (1 << 2) /* Cycle counter reset */

static inline void enable_pmu(uint32_t evtCount){
#if defined(__GNUC__) && defined __aarch64__
	evtCount &= ARMV8_PMEVTYPER_EVTCOUNT_MASK;
	__asm__ __volatile__("isb");
	/* Just use counter 0 */
	__asm__ __volatile__("msr pmevtyper0_el0, %0" : : "r" (evtCount));
	/*   Performance Monitors Count Enable Set register bit 30:1 disable, 31,1 enable */
	uint32_t r = 0;

    /* Reset counters */
    __asm__ __volatile__("mrs %0, pmcr_el0" : "=r" (r)); 
	__asm__ __volatile__("msr pmcr_el0, %0" : : "r" (r|ARMV8_PMCR_P|ARMV8_PMCR_C)); 

    /* Enable counters */
	__asm__ __volatile__("mrs %0, pmcntenset_el0" : "=r" (r));
	__asm__ __volatile__("msr pmcntenset_el0, %0" : : "r" (r|1));
#else
#error Unsupported architecture/compiler!
#endif
}

static inline uint32_t read_pmu(void){
#if defined(__GNUC__) && defined __aarch64__
    uint32_t r = 0;
	__asm__ __volatile__("mrs %0, pmevcntr0_el0" : "=r" (r)); 
	return r;
#else
#error Unsupported architecture/compiler!
#endif
}

static inline uint32_t reset_pmu(void){
#if defined(__GNUC__) && defined __aarch64__
    uint32_t r = 0;
	__asm__ __volatile__("mrs %0, pmcr_el0" : "=r" (r));
	__asm__ __volatile__("msr pmcr_el0, %0" : : "r" (r|ARMV8_PMCR_P|ARMV8_PMCR_C));
	return r;
#else
#error Unsupported architecture/compiler!
#endif
}

static inline void disable_pmu(uint32_t evtCount){
#if defined(__GNUC__) && defined __aarch64__
	/*   Performance Monitors Count Enable Set register: clear bit 0 */
	uint32_t r = 0;

	__asm__ __volatile__("mrs %0, pmcntenset_el0" : "=r" (r));
	__asm__ __volatile__("msr pmcntenset_el0, %0" : : "r" (r&&0xfffffffe));
#else
#error Unsupported architecture/compiler!
#endif
}


#endif /* ARMPMU_LIB_H */
