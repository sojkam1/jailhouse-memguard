#ifndef SIZEDEF_H
#define SIZEDEF_H

#define ARRAY_SIZE_POWER_TWO 15
#define ARRAY_SIZE_ELEMENTS (1<<ARRAY_SIZE_POWER_TWO)
#define ARRAY_SIZE (ARRAY_SIZE_ELEMENTS*2)

#ifndef PI
#define PI	3.14159265358979323846264338327950288
#endif

#define RANDOM_ARRAY_SIZE 4194304 // 32 MB

/* GEMM
    C = aAB + bC
    C - r × c
    A - r × d
    B - d × c

*/

/* Counts generated by generator */
#define TOTAL_G7 1
#define TOTAL_G4 0
#define TOTAL_G2 0
#define TOTAL_FFT 2
#define TOTAL_CONV 0
#define TOTAL_JACOBI 0
#define TOTAL_COMPATIBLE 6

#endif //SIZEDEF_H
