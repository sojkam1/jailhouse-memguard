#include <stdint.h>
#include "premdef.h"
#include "kernels.h"

#if defined(PREM_TARGET_ILP) || defined(PREM_TARGET_MUTEX)

prem_function jobs[] = {
  // 0
  {
    /* g_tran7 0.0 */
    .name = "g_tran70_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 7,
    .successors = { 1, 2, 3, 4, 5, 6, 7 },
    .func = __premized__gemm_transpose7__wrapper,
    .order_prefetch = 0,
    .order_writeback = 1,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 1
  {
    /* g_ker7 0.0 */
    .name = "g_ker70_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 2,
    .order_writeback = 3,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 2
  {
    /* g_ker7 0.1 */
    .name = "g_ker70_1",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 4,
    .order_writeback = 5,
    .parallel_id = 1,
    .kernel_id = 0,
  },
  // 3
  {
    /* g_ker7 0.2 */
    .name = "g_ker70_2",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 6,
    .order_writeback = 7,
    .parallel_id = 2,
    .kernel_id = 0,
  },
  // 4
  {
    /* g_ker7 0.3 */
    .name = "g_ker70_3",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 8,
    .order_writeback = 9,
    .parallel_id = 3,
    .kernel_id = 0,
  },
  // 5
  {
    /* g_ker7 0.4 */
    .name = "g_ker70_4",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 10,
    .order_writeback = 11,
    .parallel_id = 4,
    .kernel_id = 0,
  },
  // 6
  {
    /* g_ker7 0.5 */
    .name = "g_ker70_5",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 12,
    .order_writeback = 13,
    .parallel_id = 5,
    .kernel_id = 0,
  },
  // 7
  {
    /* g_ker7 0.6 */
    .name = "g_ker70_6",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __premized__gemm_kernel7__wrapper,
    .order_prefetch = 14,
    .order_writeback = 15,
    .parallel_id = 6,
    .kernel_id = 0,
  },
  // 8
  {
    /* random 0.1 */
    .name = "random0_1",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 7,
    .successor_quantity = 0,
    .func = __premized__random__wrapper,
    .order_prefetch = 16,
    .order_writeback = 17,
    .parallel_id = 1,
    .kernel_id = 0,
  },
  // 9
  {
    /* fft 0.0 */
    .name = "fft0_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 1,
    .successors = { 10 },
    .func = __premized__fft__wrapper,
    .order_prefetch = 18,
    .order_writeback = 19,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 10
  {
    /* fft 1.0 */
    .name = "fft1_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 0,
    .func = __premized__fft__wrapper,
    .order_prefetch = 20,
    .order_writeback = 21,
    .parallel_id = 0,
    .kernel_id = 1,
  },
  // 11
  {
    /* random 1.0 */
    .name = "random1_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 1,
    .successors = { 12 },
    .func = __premized__random__wrapper,
    .order_prefetch = 22,
    .order_writeback = 23,
    .parallel_id = 0,
    .kernel_id = 1,
  },
  // 12
  {
    /* random 2.0 */
    .name = "random2_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 13 },
    .func = __premized__random__wrapper,
    .order_prefetch = 24,
    .order_writeback = 25,
    .parallel_id = 0,
    .kernel_id = 2,
  },
  // 13
  {
    /* random 3.0 */
    .name = "random3_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 14 },
    .func = __premized__random__wrapper,
    .order_prefetch = 26,
    .order_writeback = 27,
    .parallel_id = 0,
    .kernel_id = 3,
  },
  // 14
  {
    /* random 4.0 */
    .name = "random4_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 15 },
    .func = __premized__random__wrapper,
    .order_prefetch = 28,
    .order_writeback = 29,
    .parallel_id = 0,
    .kernel_id = 4,
  },
  // 15
  {
    /* random 5.0 */
    .name = "random5_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 0,
    .func = __premized__random__wrapper,
    .order_prefetch = 30,
    .order_writeback = 31,
    .parallel_id = 0,
    .kernel_id = 5,
  },
};

#elif defined(PREM_TARGET_LEGACY)

prem_function jobs[] = {
  // 0
  {
    /* g_tran7 0.0 */
    .name = "g_tran70_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 7,
    .successors = { 1, 2, 3, 4, 5, 6, 7 },
    .func = __legacy__gemm_transpose7__wrapper,
    .order_prefetch = 0,
    .order_writeback = 1,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 1
  {
    /* g_ker7 0.0 */
    .name = "g_ker70_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 2,
    .order_writeback = 3,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 2
  {
    /* g_ker7 0.1 */
    .name = "g_ker70_1",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 4,
    .order_writeback = 5,
    .parallel_id = 1,
    .kernel_id = 0,
  },
  // 3
  {
    /* g_ker7 0.2 */
    .name = "g_ker70_2",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 6,
    .order_writeback = 7,
    .parallel_id = 2,
    .kernel_id = 0,
  },
  // 4
  {
    /* g_ker7 0.3 */
    .name = "g_ker70_3",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 8,
    .order_writeback = 9,
    .parallel_id = 3,
    .kernel_id = 0,
  },
  // 5
  {
    /* g_ker7 0.4 */
    .name = "g_ker70_4",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 10,
    .order_writeback = 11,
    .parallel_id = 4,
    .kernel_id = 0,
  },
  // 6
  {
    /* g_ker7 0.5 */
    .name = "g_ker70_5",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 12,
    .order_writeback = 13,
    .parallel_id = 5,
    .kernel_id = 0,
  },
  // 7
  {
    /* g_ker7 0.6 */
    .name = "g_ker70_6",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 8 },
    .func = __legacy__gemm_kernel7__wrapper,
    .order_prefetch = 14,
    .order_writeback = 15,
    .parallel_id = 6,
    .kernel_id = 0,
  },
  // 8
  {
    /* random 0.1 */
    .name = "random0_1",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 7,
    .successor_quantity = 0,
    .func = __legacy__random__wrapper,
    .order_prefetch = 16,
    .order_writeback = 17,
    .parallel_id = 1,
    .kernel_id = 0,
  },
  // 9
  {
    /* fft 0.0 */
    .name = "fft0_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 1,
    .successors = { 10 },
    .func = __legacy__fft__wrapper,
    .order_prefetch = 18,
    .order_writeback = 19,
    .parallel_id = 0,
    .kernel_id = 0,
  },
  // 10
  {
    /* fft 1.0 */
    .name = "fft1_0",
    .type = PREM_JOB_TYPE_PREDICTABLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 0,
    .func = __legacy__fft__wrapper,
    .order_prefetch = 20,
    .order_writeback = 21,
    .parallel_id = 0,
    .kernel_id = 1,
  },
  // 11
  {
    /* random 1.0 */
    .name = "random1_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 0,
    .successor_quantity = 1,
    .successors = { 12 },
    .func = __legacy__random__wrapper,
    .order_prefetch = 22,
    .order_writeback = 23,
    .parallel_id = 0,
    .kernel_id = 1,
  },
  // 12
  {
    /* random 2.0 */
    .name = "random2_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 13 },
    .func = __legacy__random__wrapper,
    .order_prefetch = 24,
    .order_writeback = 25,
    .parallel_id = 0,
    .kernel_id = 2,
  },
  // 13
  {
    /* random 3.0 */
    .name = "random3_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 14 },
    .func = __legacy__random__wrapper,
    .order_prefetch = 26,
    .order_writeback = 27,
    .parallel_id = 0,
    .kernel_id = 3,
  },
  // 14
  {
    /* random 4.0 */
    .name = "random4_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 1,
    .successors = { 15 },
    .func = __legacy__random__wrapper,
    .order_prefetch = 28,
    .order_writeback = 29,
    .parallel_id = 0,
    .kernel_id = 4,
  },
  // 15
  {
    /* random 5.0 */
    .name = "random5_0",
    .type = PREM_JOB_TYPE_COMPATIBLE,
    .predecesor_done = 0,
    .predecesor_quantity = 1,
    .successor_quantity = 0,
    .func = __legacy__random__wrapper,
    .order_prefetch = 30,
    .order_writeback = 31,
    .parallel_id = 0,
    .kernel_id = 5,
  },
};

#else
#error PREM_TARGET_xxx has to be defined (PREM_TARGET_MUTEX, PREM_TARGET_ILP, PREM_TARGET_LEGACY)
#endif // defined(PREM_TARGET_xxx)
