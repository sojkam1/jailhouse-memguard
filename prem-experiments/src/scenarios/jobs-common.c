#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE
/* Include jobs definitions according to selected scenario */

//#ifdef PREM_SCENARIO_PATH
/*
#if PREM_SCENARIO == 42mul+1fft+1fft+3rand
#include <42mul+1fft+1fft+3rand/jobs.c>
#elif PREM_SCENARIO == "42mul+1fft+3rand+2rand"
#include <42mul+1fft+3rand+2rand/jobs.c>
#elif PREM_SCENARIO == 42mul+1fft+5rand
#include <42mul+1fft+5rand/jobs.c>
#elif PREM_SCENARIO == 7mul+1fft+5rand
#include <7mul+1fft+5rand/jobs.c>
#else
#error Unknown PREM_SCENARIO
#endif //switch PREM_SCENARIO
*/

//#include <PREM_SCENARIO_PATH/jobs.c>
#include <jobs.c>

//#else //PREM_SCENARIO not defined
//#error PREM_SCENARIO_PATH has to be defined
//#endif

#define PREM_JOB_QUANTITY (sizeof(jobs)/sizeof(jobs[0]))

prem_function * jobs_queue[PREM_JOB_QUANTITY];

const int prem_job_q = PREM_JOB_QUANTITY;
