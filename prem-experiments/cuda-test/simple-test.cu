#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>

typedef enum {
    PREM_PHASE_CPU_PREF = 0,
    PREM_PHASE_CPU_COMP = 1,
    PREM_PHASE_CPU_WRBA = 2,
    PREM_PHASE_CPU_NONE = 3,
    PREM_PHASE_GPU_PREF = 4,
    PREM_PHASE_GPU_COMP = 5,
    PREM_PHASE_GPU_WRBA = 6,
    PREM_PHASE_GPU_NONE = 7
} prem_phase;

const char * const prem_phase_string [] = {"CPU PREF", "CPU COMP", "CPU WRBA", "CPU NONE", "GPU PREF", "GPU COMP", "GPU WRBA", "GPU NONE"};

typedef struct prem_log{
    uint64_t time;
    prem_phase phase;
} prem_log;

#define LOG_SIZE 2048

static __attribute__((always_inline)) inline uint64_t clock_ns(){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec * 1000000000 + ts.tv_nsec;
}

static __attribute__((always_inline)) inline void delay_us(uint64_t us){
    uint64_t current, end;
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    end = ts.tv_sec * 1000000000 + ts.tv_nsec + us * 1000;

    do{
        clock_gettime(CLOCK_MONOTONIC, &ts);
        current = ts.tv_sec * 1000000000 + ts.tv_nsec;
    }while(current < end);
}

__device__ static __attribute__((always_inline)) inline uint64_t gpu_clock_ns(){
    uint64_t x;
    asm volatile("mov.u64 %0, %%globaltimer;" : "=l"(x));
    return x;
}

#define SIZE_R 256
#define SIZE_C 8
#define SIZE_D 8

__global__
void prem(double *A, double *BB, double *C, volatile int * counter, prem_log * log, volatile int * log_count)
{
    uint64_t start = gpu_clock_ns();
    //uint64_t prefetch, compute, writeback, exit;
    int it;

    __shared__ double A_sh[SIZE_R * SIZE_D];
    __shared__ double BB_sh[SIZE_C * SIZE_D];
    __shared__ double C_sh[SIZE_R * SIZE_C];

    log[*log_count].phase = PREM_PHASE_GPU_NONE;
    log[*log_count].time = start;
    *log_count = *log_count + 1;

    /* PREFETCH */
    if(threadIdx.x == 0){
        //while(*counter < 1);
        //prefetch = gpu_clock_ns();
        log[*log_count].phase = PREM_PHASE_GPU_PREF;
        log[*log_count].time = gpu_clock_ns();
        *log_count = *log_count + 1;


    }

    __syncthreads();

    for (int i = 0; i < ceil((SIZE_C * SIZE_D)/(double)blockDim.x); i+=1) {
        it = i * (blockDim.x) + threadIdx.x;
        if(it<SIZE_C * SIZE_D){
            BB_sh[it] = BB[it];;
        }
    }

    for (int i = 0; i < ceil((SIZE_R * SIZE_D)/(double)blockDim.x); i+=1) {
        it = i * (blockDim.x) + threadIdx.x;
        if(it<SIZE_R * SIZE_D){
            A_sh[it] = A[it];
        }
    }

    //*counter = *counter + 1;

    __syncthreads();

    /* COMPUTE */
    if(threadIdx.x == 0){
        //while(*counter < 3);
        //compute = gpu_clock_ns();
        log[*log_count].phase = PREM_PHASE_GPU_COMP;
        log[*log_count].time = gpu_clock_ns();
        *log_count = *log_count + 1;
    }

    //__syncthreads();

    /* COMPUTE */
    double sum = 0;
    for (int i = 0; i < ceil(SIZE_R/(double)blockDim.x); i++) {
        it = i * (blockDim.x) + threadIdx.x;
        for (int j = 0; j < SIZE_C; j++) {
            sum = 0;
            for (int k = 0; k < SIZE_D; k++) {
                sum += A_sh[it*SIZE_D+k]*BB_sh[j*SIZE_D+k];
            }
            C_sh[it*SIZE_C+j] = sum;
        }
    }

    __syncthreads();

    //*counter = *counter + 1;

    /* WRITEBACK */
    if(threadIdx.x == 0){
        //while(*counter < 5);
        //writeback = gpu_clock_ns();
        log[*log_count].phase = PREM_PHASE_GPU_WRBA;
        log[*log_count].time = gpu_clock_ns();
        *log_count = *log_count + 1;
    }

    __syncthreads();
    for (int i = 0; i < ceil((SIZE_R * SIZE_C)/(double)blockDim.x); i+=1) {
        it = i * (blockDim.x) + threadIdx.x;
        if(it<SIZE_R * SIZE_C){
            C[it] = C_sh[it];
        }
    }
    //*counter = *counter + 1;

    if(threadIdx.x == 0){
        //exit = gpu_clock_ns();
        log[*log_count].phase = PREM_PHASE_GPU_NONE;
        log[*log_count].time = gpu_clock_ns();
        *log_count = *log_count + 1;
    }
}

int main(void)
{
    volatile int * counter;
    double *A, *BB, *C;
    uint64_t start;
    uint64_t stop;

    prem_log * log;
    volatile int * log_count;

    // Allocate Unified Memory – accessible from CPU or GPU
    cudaMallocManaged(&A, SIZE_R*SIZE_D*sizeof(double));
    cudaMallocManaged(&BB, SIZE_D*SIZE_C*sizeof(double));
    cudaMallocManaged(&C, SIZE_R*SIZE_C*sizeof(double));

    cudaError_t ret = cudaHostAlloc( (void**)&counter, sizeof(int), cudaHostAllocMapped);
    if (ret != cudaSuccess) {
        fprintf(stderr,"GPU: %s %s %d\n", cudaGetErrorString(ret), __FILE__, __LINE__);
    }
    ret = cudaHostAlloc( (void**)&log, LOG_SIZE*sizeof(prem_log), cudaHostAllocMapped);
    if (ret != cudaSuccess) {
        fprintf(stderr,"GPU: %s %s %d\n", cudaGetErrorString(ret), __FILE__, __LINE__);
    }
    ret = cudaHostAlloc( (void**)&log_count, sizeof(int), cudaHostAllocMapped);
    if (ret != cudaSuccess) {
        fprintf(stderr,"GPU: %s %s %d\n", cudaGetErrorString(ret), __FILE__, __LINE__);
    }

    // initialize x and y arrays on the host
    for (int i = 0; i < SIZE_R*SIZE_D; i++) {
        A[i] = i*0.5;
    }

    for (int i = 0; i < SIZE_D*SIZE_C; i++) {
        BB[i] = i*0.5;
    }

    for (int i = 0; i < SIZE_R*SIZE_C; i++) {
        C[i] = 0;
    }

    start = clock_ns();

    *counter = 0;
    __asm__ __volatile__("dc cvac, %0\n" : : "r" (counter) : "memory");

    printf("GOGOGO\n");

    int threadsPerBlock = 128;
    int blocksPerGrid = 1;
    prem<<<blocksPerGrid, threadsPerBlock>>>(A, BB, C, counter, log, log_count);

    for(int i=0; i<5; i++){
        *counter = *counter + 1;
        __asm__ __volatile__("dc cvac, %0\n" : : "r" (counter) : "memory");
        printf("tick %d\n", *counter);
    }

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();

    stop = clock_ns();

    // Check for errors (all values should be 3.0f)
    printf("CPU started at: %lu stopped at: %lu\n", start, stop);
    printf("Time: %f us\n", (stop-start)/1000.0);

    uint64_t last_time = log[0].time;
    for(int i=0; i<*log_count; i++){
        printf("Log: %20s: %15.3f\n",prem_phase_string[log[i].phase], (log[i].time-last_time)/1000.0);
        last_time = log[i].time;
    }

    /*
    printf("A=\n");
    for(int i=0; i<SIZE_R; i++){
        for(int j=0; j<SIZE_D; j++){
            printf("%9.1f ", A[i*SIZE_D+j]);
        }
        printf("\n");
    }

    printf("BB=\n");
    for(int i=0; i<SIZE_D; i++){
        for(int j=0; j<SIZE_C; j++){
            printf("%9.1f ", BB[i*SIZE_C+j]);
        }
        printf("\n");
    }
    */

    printf("C=\n");
    for(int i=0; i<SIZE_R; i++){
        for(int j=0; j<SIZE_C; j++){
            printf("%9.1f ", C[i*SIZE_C+j]);
        }
        printf("\n");
    }

    // Free memory
    cudaFree(A);
    cudaFree(BB);
    cudaFree(C);

    return 0;
}
