/* Run this as: for i in $(seq 0 5); do prem-test $i & done */

#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <err.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef SYS_prem_guard_set
#define SYS_prem_guard_set 792
#endif

enum prem_phase {
	PREM_COMPATIBLE = 0,
	PREM_MEMORY	= 1,
	PREM_COMPUTE	= 2,
};

long prem_guard_set(enum prem_phase phase,
		    unsigned long memory_budget,
		    unsigned long timeout)
{
	return syscall(SYS_prem_guard_set, phase, memory_budget, timeout);
}

int main(int argc, char *argv[])
{
	cpu_set_t set;
	int cpu = 0;

	if (argc > 1)
		cpu = atoi(argv[1]);

	/* Ensure that memory phase starts and ends on the same CPU */
	CPU_ZERO(&set);
	CPU_SET(cpu, &set);
	if (sched_setaffinity(getpid(), sizeof(set), &set) < 0)
		err(1, "sched_setaffinity");
	printf("Pinned to CPU %d\n", cpu);

	prem_guard_set(PREM_MEMORY, 22, 33);
	for (int i = 0; i < 100; i++)
		printf("Memory phase PID %d\n", getpid());
	prem_guard_set(PREM_COMPATIBLE, 44, 55);
	return 0;
}
