exec >&2

: ${DESTDIR:?}

redo-ifchange jailhouse

make -C ../jailhouse install KDIR=$PWD/kernel-4.4/ ARCH=arm64 DESTDIR=$(realpath "${DESTDIR}") prefix=/usr
mkdir -p ${DESTDIR}/jailhouse/configs/ ${DESTDIR}/jailhouse/demos/
# cp ../jailhouse/inmates/demos/arm64/my-demo.bin rootfs-overlay/jailhouse/demos/
cp ../jailhouse/configs/arm64/jetson-tx2.cell ${DESTDIR}/jailhouse/configs/
cp ../jailhouse/configs/arm64/jetson-tx2-demo.cell ${DESTDIR}/jailhouse/demos/
cp ../jailhouse/inmates/demos/arm64/*.bin ${DESTDIR}/jailhouse/demos/
