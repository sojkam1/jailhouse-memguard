CC=/opt/OSELAS.Toolchain-2014.12.2/aarch64-v8a-linux-gnu/gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized/bin/aarch64-v8a-linux-gnu-gcc
CFLAGS="-Wall -O2 -g -std=gnu99"
LIBS=-lpthread

for i in prem-test memguard-test; do
    redo-ifchange ../../test/$i.c
    $CC $CFLAGS ../../test/$i.c $LIBS -o $i
done
