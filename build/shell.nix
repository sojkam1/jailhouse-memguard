# Usage: run "nix-shell --pure" and then "redo -j$(nproc)"

{ pkgs ? import (fetchTarball https://github.com/NixOS/nixpkgs/archive/refs/heads/release-21.05.tar.gz) {} }:
let
  oselas_toolchain-aarch64-v8a-linux-gnu = pkgs.stdenv.mkDerivation rec {
    pname = "oselas.toolchain-aarch64-v8a-linux-gnu";
    version = "2014.12.2";
    src = builtins.fetchurl {
      url = "https://debian.pengutronix.de/debian/pool/main/o/oselas.toolchain/oselas.toolchain-2014.12.2-aarch64-v8a-linux-gnu-gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized_2014.12.2_amd64.deb";
      # date = 2020-11-30T10:16:56+0100;
      sha256 = "187ghhgin37h1bflg3qy0nv8ykxlfvkcx9wnmlzhmvpqlzdrv2gq";
    };
    nativeBuildInputs = with pkgs; [ dpkg makeWrapper ];
    buildCommand = "dpkg-deb -x $src $out";
  };
in
(pkgs.buildFHSUserEnvBubblewrap {
  name = "jailhouse-tx2-build";
  targetPkgs = pkgs: (with pkgs; [
    redo-apenwarr git
    # Kconfig needs:
    pkg-config ncurses ncurses.dev
    # Linux kernel needs:
    oselas_toolchain-aarch64-v8a-linux-gnu
    gcc flex bison openssl bc elfutils gnumake42
    # Buildroot needs
    file unzip which rsync cpio wget perl flock binutils-unwrapped zlib getopt


    # keep this line if you use bash
    bashInteractive
  ]);
  extraBuildCommands = ''
    # Not needed since 21.11 (https://github.com/NixOS/nixpkgs/commit/b681ad32540c5bcb93d3cb98dfd25f22f2eb5503)
    ln -sf ${oselas_toolchain-aarch64-v8a-linux-gnu}/opt opt
  '';
  #runScript = "redo -j$(nproc)";
}).env
