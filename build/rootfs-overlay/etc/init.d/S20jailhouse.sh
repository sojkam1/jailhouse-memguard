#!/bin/sh

if ! grep -q -w jailhouse /proc/cmdline; then
    echo "Jailhouse not enabled"
    return 1
fi

case "$1" in
    start)
	printf "Starting Jailhouse: "
	/sbin/modprobe jailhouse
	# Switch off Denver cores
	echo 0 > /sys/devices/system/cpu/cpu1/online
	echo 0 > /sys/devices/system/cpu/cpu2/online
	/usr/sbin/jailhouse enable /jailhouse/configs/jetson-tx2.cell
	[ $? = 0 ] && echo "OK" || echo "FAIL"
	;;
    stop)
	printf "Stopping Jailhouse: "
	/usr/sbin/jailhouse disable /jailhouse/configs/jetson-tx2.cell
	/sbin/rmmod jailhouse
	[ $? = 0 ] && echo "OK" || echo "FAIL"
	;;
esac
