exec >&2

(d=../jailhouse/; cd $d && git ls-files | sed -e s,^,$d,) | xargs redo-ifchange
redo-ifchange kernel-4.4/all jailhouse-config
make -C ../jailhouse KDIR=$PWD/kernel-4.4/ ARCH=arm64 prefix=/usr
