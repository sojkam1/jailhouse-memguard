exec >&2
redo-ifchange jailhouse-install.do test/all

DESTDIR=$PWD/rootfs-overlay
. ./jailhouse-install.do

install -v $(find test -type f -executable) "$DESTDIR"/jailhouse
